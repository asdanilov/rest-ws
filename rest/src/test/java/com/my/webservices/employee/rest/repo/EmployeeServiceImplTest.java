package com.my.webservices.employee.rest.repo;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.my.webservices.employee.rest.domain.EmployeeEntity;
import com.my.webservices.employee.rest.services.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class EmployeeServiceImplTest {

	@Autowired
    EmployeeDataOnDemand dod;

	@Autowired
    EmployeeService employeeService;
	
	@Test
	public void testFindEmloyeeEntries() {
		
		List<EmployeeEntity> list = employeeService.findEmloyeeEntries(0, 10);
		Assert.assertNotNull("testFindEmloyeeEntries for EmployeeEntity returned NULL", list);
		Assert.assertTrue("testFindEmloyeeEntries for EmployeeEntity returned nothing found", list.size() > 0);
		
	}

	@Test
	public void testFindByDepartment() {
		EmployeeEntity employeeEntity = dod.getRandomEmployee();
		Assert.assertNotNull("Data on demand for 'EmployeeEntity' failed to initialize", employeeEntity);
		
		List<EmployeeEntity> list = employeeService.findByDepartment(employeeEntity.getDepartment());
		Assert.assertNotNull("testFindByDepartment for EmployeeEntity returned NULL", list);
		Assert.assertTrue("testFindByDepartment for EmployeeEntity returned nothing found", list.size() > 0);
	}

	@Test
	public void testFindOne() {
		
		EmployeeEntity employeeEntity = dod.getRandomEmployee();
		Assert.assertNotNull("Data on demand for 'EmployeeEntity' failed to initialize", employeeEntity);
		
		Long id = employeeEntity.getId();
		Assert.assertNotNull("Id provision for employee failed", id);
		
		employeeEntity = employeeService.findOne(id);
		Assert.assertNotNull("findOne method for EmployeeEntity returned NULL", employeeEntity);
		Assert.assertEquals("findOne method for EmployeeEntity returned NULL", (Long)id, (Long)employeeEntity.getId());

	}

}