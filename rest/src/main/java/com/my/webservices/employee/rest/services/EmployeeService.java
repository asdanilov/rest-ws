package com.my.webservices.employee.rest.services;

import java.util.List;

import com.my.webservices.employee.rest.domain.EmployeeEntity;

/**
 * Service for Employee
 * 
 * @author Alex
 *
 */
public interface EmployeeService {
	
	public List<EmployeeEntity> findEmloyeeEntries(int min, int max);
	
	List<EmployeeEntity> findByDepartment(String department);
	
	EmployeeEntity findOne(Long id);
		
}
