package com.my.webservices.employee.rest.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base entity to hold id - to be extended by other entities
 * @author Alex
 *
 */
@MappedSuperclass
public class BaseEntity implements Serializable{

	private static final long serialVersionUID = 1887719384289365878L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}	
	
}
