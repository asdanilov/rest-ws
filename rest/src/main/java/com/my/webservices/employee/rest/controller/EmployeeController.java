package com.my.webservices.employee.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.my.webservices.employee.rest.domain.EmployeeEntity;
import com.my.webservices.employee.rest.services.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@RequestMapping("/getEmployeeById")
	public EmployeeEntity getEmployeeById(@RequestParam(value = "id") Long id){
		return service.findOne(id); 
	}
	
	@RequestMapping("/getEmployeesByDepartment")
//	public List<EmployeeEntity> getEmployeesByDepartment(@RequestParam(value = "department") MultiValueMap parameters)
	
	public List<EmployeeEntity> getEmployeesByDepartment(@RequestParam(value = "department") String department){
		return service.findByDepartment(department);
	}
}