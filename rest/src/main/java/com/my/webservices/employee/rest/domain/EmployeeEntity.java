package com.my.webservices.employee.rest.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Employee entity
 * @author Alex
 *
 */

@Entity
@Table(name="employees")
public class EmployeeEntity extends BaseEntity  {

	private static final long serialVersionUID = -3785141296463500058L;
	
    @Column(name="firstname")
	private String firstName;
    @Column(name="lastname")
	private String lastName;
    @Column(name="suname")
	private String suName;
    @Column(name="birthdate")
	private Date birthDate;
    @Column(name="department")
	private String department;
    @Column(name="phone")
	private String phone;
    @Column(name="email", unique = true)
	private String email;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuName() {
		return suName;
	}
	public void setSuName(String suName) {
		this.suName = suName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
